### récupérer secret argocd pour le password

kubectl -n argocd get secret argocd-initial-admin-secret -o jsonpath="{.data.password}" | base64 -d; echo

URL: faire `minikube ip` et chercher le port du service argocd-server qui doit être en NodePort.

### récupérer secret pour elastic password

PASSWORD=$(kubectl get secret quickstart-es-elastic-user -n logging -o go-template='{{.data.elastic | base64decode}}')

Faire redirection port `kubectl port-forward service/quickstart-es-http -n logging 9200`

### récupérer secret pour kibana password

kubectl get secret quickstart-es-elastic-user -n logging -o=jsonpath='{.data.elastic}' | base64 --decode; echo

`kubectl port-forward service/quickstart-kb-http -n logging 5601`

9VO43Fq58QZUS0m6C72RG3eb

## Ce que j'ai fait sur linux

install VM ubuntu

sudo apt update
sudo apt install openssh-server
sudo systemctl enable ssh
sudo systemctl start ssh

trouver ip avec ip addr puis ssh user@ip depuis WSL

Généré une clé ssh avec ssh-keygen depuis gitbash puis ajouté sur la VM ubuntu avec `ssh-copy-id vboxuser@192.168.1.7`

Sur WSL j'ai du ajouter la clé ssh a `~/.ssh/id_rsa` 

### Ansible

ansible myhosts -m ping -i inventory.ini
